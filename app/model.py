import numpy as np
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier


class Model:
    def __init__(self, classifier):
        '''
        Constructor to set classifier (rf or svm)
        '''
        self.classifier = classifier
        

    def preprocess_data(self, X_train):
        '''
        Mean normalize the data and return X_train
        '''
        self.mean_train = np.mean(X_train, axis = 0)
        self.std_train = np.std(X_train, axis = 0)
        X_train = (X_train - self.mean_train) / self.std_train

        return X_train


    def train(self, X_train, y_train):
        '''
        Train the model for rf or svm
        '''
        X_train = self.preprocess_data(X_train)
        
        if self.classifier == 'rf':
            self.rf = RandomForestClassifier(max_depth = 5, max_leaf_nodes = 100, max_features = 7, n_estimators = 20, n_jobs = -1)
            self.rf.fit(X_train, y_train)

        elif self.classifier == 'svm':
            self.svm = SVC()
            self.svm.fit(X_train, y_train)


    def predict(self, X_test):
        '''
        Predict for rf or svm and return predicted labels
        '''
        self.X_test = (X_test - self.mean_train) / self.std_train

        if self.classifier == 'rf':
            print 'predicting'
            y_pred = self.rf.predict(X_test)
            print 'predicted'

        elif self.classifier == 'svm':
            y_pred = self.svm.predict(X_test)

        return y_pred
