import numpy as np
import pandas as pd


def load_data(path, delim = ';'):
    '''
    Load data from the specified CSV and return X and y labels
    '''
    data_df = pd.read_csv(path, delimiter = delim)
    
    X = categorical_to_numerical(data_df[ data_df.columns[:-1] ])
    y = data_df[ data_df.columns[-1] ]

    return X, y

def categorical_to_numerical(data):
    '''
    Convert categorical to numerical by assigning each class a number
    '''
    for col in data.columns:
        if data[ col ].dtype != np.int64:
            for idx, val in enumerate(data[ col ].unique()):
                data.loc[ data[ col ] == val, col ] = idx

    return data
