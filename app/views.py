from app import app
from flask import render_template
from sklearn.model_selection import train_test_split
from tools import load_data
from model import Model


def process():
    '''
    Call methods from model to train the model, predict, and generate output
    '''
    
    X, y = load_data(path='Bank Marketing Dataset/bank-full.csv')
#     X, y = load_data(path='Bank Marketing Dataset/bank.csv')
    print "Data loaded"
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)
    
    print "Training started"
    model = Model('svm')
    model.train(X_train, y_train)
    print "Training ended"

    y_pred = model.predict(X_test)
    accuracy = sum(y_pred == y_test) / float(y_test.shape[0])
    print "Accuracy calculated"

    output = {
        'train_examples': X_train.shape[0],
        'test_examples': X_test.shape[0],
        'n_features': X_train.shape[1],
        'accuracy': round(accuracy * 100, 2)
    }
    
    return output


output = process()


@app.route("/", methods = [ 'GET' ])
def index():
    '''
    render HTML page and pass the output
    '''

    return render_template('index.html', output = output)

